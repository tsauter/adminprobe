package storages

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"gitlab.com/tsauter/adminprobe/middleware"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	// UsersTable is the table used to store the collected Users
	UsersTable = "users"
	// GroupsTable is the table used to store the collected Groups and their members
	GroupsTable = "groups"
)

// ComputerUser is the structure stored in MongoDB
type ComputerUser struct {
	Computer    string
	Name        string
	SID         string
	Disabled    bool
	WhenChanged time.Time
}

// ComputerGroup is the structure stored in MongoDB
type ComputerGroup struct {
	Computer    string
	Name        string
	LocName     string
	SID         string
	Entry       string
	EntryType   string
	WhenChanged time.Time
}

// A MongoStore is the MongoDB backend storage to permanently
// store the incoming data.
type MongoStore struct {
	mgoSession *mgo.Session
	mgoDB      *mgo.Database
}

// NewMongoStore returns a preconfigured MongoStore instance.
func NewMongoStore(dbhost string, dbname string) (*MongoStore, error) {
	// connect to MongoDB
	session, err := mgo.Dial(dbhost)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to MongoDB: %s: %v", dbhost, err)
	}

	// mongo settings and database selection
	session.SetMode(mgo.Monotonic, true)
	db := session.DB(dbname)

	return &MongoStore{
		mgoSession: session,
		mgoDB:      db,
	}, nil
}

// Close closes the opened connections.
func (s *MongoStore) Close() {
	s.mgoSession.Close()
}

// GetSuspiciousAdminUsers print a list of collected users, that a part of the local
// Administrators group. Some uses may be ignored (taken as parameter).
func (s *MongoStore) GetSuspiciousAdminUsers(ignoreList *[]string) error {
	// select collecton
	mgoColl := s.mgoDB.C(GroupsTable)

	var result []ComputerGroup
	err := mgoColl.Find(
		bson.M{
			"name": "Administrators",
			//"entry": bson.M{"$in": []string{"MYCOMPUTER\\Administrator", "b"}},
		}).Sort("computer", "name", "entry").All(&result)
	if err != nil {
		return fmt.Errorf("failed to query MongoDB: %v", err)
	}

	for _, dbcolumn := range result {
		// create the final ignore list, all placeholders have been replaced
		replacemap := map[string]string{
			"$computer": dbcolumn.Computer,
		}
		finalIgnores, err := buildRegexList(ignoreList, &replacemap)
		if err != nil {
			return fmt.Errorf("cannot use ignore list: %v", err)
		}

		// match the entries against the ignore list and skip if a match was found
		if isRegexListMatch(dbcolumn.Entry, finalIgnores) {
			continue
		}

		fmt.Printf("%-20s\t%s\n", dbcolumn.Computer, dbcolumn.Entry)
	}
	return nil
}

// GetSuspiciousAdditionalUsers prints a list of collected users, stripped by all users
// which should be ignored (taken as parameter).
func (s *MongoStore) GetSuspiciousAdditionalUsers(ignoreList *[]string) error {
	// select collecton
	mgoColl := s.mgoDB.C(UsersTable)

	var result []ComputerUser
	err := mgoColl.Find(nil).Sort("computer", "name").All(&result)
	if err != nil {
		return fmt.Errorf("failed to query MongoDB: %v", err)
	}

	for _, dbcolumn := range result {
		// create the final ignore list, all placeholders have been replaced
		replacemap := map[string]string{
			"$computer": dbcolumn.Computer,
		}
		finalIgnores, err := buildRegexList(ignoreList, &replacemap)
		if err != nil {
			return fmt.Errorf("cannot use ignore list: %v", err)
		}

		// match the entries against the ignore list and skip if a match was found
		if isRegexListMatch(dbcolumn.Name, finalIgnores) {
			continue
		}

		fmt.Printf("%-20s\t%-25s\t%v\n", dbcolumn.Computer, dbcolumn.Name, dbcolumn.Disabled)
	}
	return nil
}

// StoreUsersData deleted all existing entries for the computer and
// then store all users into the MongoDB.
func (s *MongoStore) StoreUsersData(data *middleware.UsersData) error {
	// select collecton
	mgoColl := s.mgoDB.C(UsersTable)

	// create a timestamp
	timestamp := time.Now().UTC()

	// delete existing records
	mgoColl.RemoveAll(bson.M{"computer": strings.ToLower(data.Hostname)})

	// walk over Users and add to the collection
	for _, user := range data.Users {
		err := mgoColl.Insert(ComputerUser{
			Computer:    strings.ToLower(data.Hostname),
			Name:        user.Name,
			SID:         user.SID,
			Disabled:    user.Disabled,
			WhenChanged: timestamp,
		})
		if err != nil {
			return fmt.Errorf("failed to insert user into MongoDB: %v", err)
		}
	}

	return nil
}

// StoreGroupsData deleted all existing entries for the computer and
// then store all groups into the MongoDB.
func (s *MongoStore) StoreGroupsData(data *middleware.GroupsData) error {
	// select collecton
	mgoColl := s.mgoDB.C(GroupsTable)

	// create a timestamp
	timestamp := time.Now().UTC()

	// delete existing records
	mgoColl.RemoveAll(bson.M{"computer": strings.ToLower(data.Hostname)})

	// walk over Groups
	for _, group := range data.Groups {
		// walk over member users and store them into the collection
		for _, membuser := range group.MemberUsers {
			err := mgoColl.Insert(ComputerGroup{
				Computer:    strings.ToLower(data.Hostname),
				Name:        group.Name,
				LocName:     group.LocName,
				SID:         group.SID,
				Entry:       fmt.Sprintf("%s\\%s", membuser.Domain, membuser.Name),
				EntryType:   "user",
				WhenChanged: timestamp,
			})
			if err != nil {
				return fmt.Errorf("failed to insert group members into MongoDB: %v", err)
			}
		}
		// walk over member groups and store them into the collection
		for _, membgroup := range group.MemberGroups {
			err := mgoColl.Insert(ComputerGroup{
				Computer:    strings.ToLower(data.Hostname),
				Name:        group.Name,
				LocName:     group.LocName,
				SID:         group.SID,
				Entry:       fmt.Sprintf("%s\\%s", membgroup.Domain, membgroup.Name),
				EntryType:   "group",
				WhenChanged: time.Now().UTC(),
			})
			if err != nil {
				return fmt.Errorf("failed to insert group members into MongoDB: %v", err)
			}
		}

	}

	return nil
}

// buildRegexList takes a list of strings, replace variables inside these strings
// and then return the list as a compiled regex list.
func buildRegexList(ignoreList *[]string, replacements *map[string]string) (*[]regexp.Regexp, error) {
	var finalIgnores []regexp.Regexp

	for _, e := range *ignoreList {
		// replace all ocurences inside the string with the value
		for k, v := range *replacements {
			e = strings.Replace(e, k, v, -1)
		}
		// compile regex and add to the list
		re, err := regexp.Compile(e)
		if err != nil {
			return nil, fmt.Errorf("invalid regex in ingore list: %s: %v", e, err)
		}
		finalIgnores = append(finalIgnores, *re)
	}

	return &finalIgnores, nil
}

// stringInSlice returns true if the string is in the specified list
func stringInSlice(a string, list []string) bool {
	a = strings.ToLower(a)
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// isRegexListMatch matches the string against a list of regexes. Return true
// if a match was found.
func isRegexListMatch(a string, relist *[]regexp.Regexp) bool {
	for _, re := range *relist {
		if re.MatchString(a) {
			return true
		}
	}
	return false
}
