FROM golang:latest
MAINTAINER Thorsten Sauter <t.sauter@viastore.com>
ENV DOCKER_BUILD 0.2

RUN mkdir -p /app

WORKDIR /go/src/gitlab.com/tsauter/adminprobe

RUN go get -u github.com/gorilla/mux gopkg.in/mgo.v2

COPY ./ ./
RUN cd cmd/apiserver && go build -o /app/apiserver .

WORKDIR /app
VOLUME /conf
EXPOSE 9999

ENTRYPOINT ["/app/apiserver"]
