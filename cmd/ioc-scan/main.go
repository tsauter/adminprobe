package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/tsauter/adminprobe/storages"
	"gopkg.in/yaml.v2"
)

var (
	store *storages.MongoStore
)

// Config holds the configuration settings.
type Config struct {
	MongoHost        string   `yaml:"MongoHost"`
	MongoDB          string   `yaml:"MongoDB"`
	IgnoreAdminsList []string `yaml:"IgnoreAdminsList"`
	IgnoreUsersList  []string `yaml:"IgnoreUsersList"`
}

func main() {
	// create command line
	configfile := flag.String("config", "ioc-scan.yaml", "Use this configuration file")
	flag.Parse()

	// read the configuration file
	var config Config
	rawconfig, err := ioutil.ReadFile(*configfile)
	if err != nil {
		fmt.Printf("Failed to read YAML configuration: %v\n", err)
		os.Exit(1)
	}
	err = yaml.Unmarshal(rawconfig, &config)
	if err != nil {
		fmt.Printf("Failed to load YAML configuration: %v\n", err)
		os.Exit(1)
	}
	//fmt.Printf("%#v\n", config)

	// create a backend to store data, MongoDB in this case
	store, err = storages.NewMongoStore(config.MongoHost, config.MongoDB)
	if err != nil {
		fmt.Printf("Failed to initialize MongoDB backend: %v\n", err)
		os.Exit(1)
	}
	defer store.Close()

	// search for Administrator that are not allowed
	fmt.Printf("\n--> Suspicious Admin Users:\n")
	err = store.GetSuspiciousAdminUsers(&config.IgnoreAdminsList)
	if err != nil {
		fmt.Printf("Failed to read MongoDB backend: %v\n", err)
		os.Exit(1)
	}

	// search for Users that are not allowed
	fmt.Printf("\n--> Suspicious additional Users:\n")
	err = store.GetSuspiciousAdditionalUsers(&config.IgnoreUsersList)
	if err != nil {
		fmt.Printf("Failed to read MongoDB backend: %v\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}
