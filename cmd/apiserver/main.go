package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/tsauter/adminprobe/middleware"
	"gitlab.com/tsauter/adminprobe/storages"
)

var (
	store  *storages.MongoStore
	apikey string
)

// updateUsers take UsersData as JSON and store in backend
func updateUsers(w http.ResponseWriter, r *http.Request) {
	// validate API key
	apikeyClient := r.Header.Get("X-API-Key")
	if apikeyClient == "" || apikeyClient != apikey {
		log.Printf("received users data with invalid API key\n")
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "Invalid API key.")
		return
	}

	// read POST data and convert from JSON to struct
	var usersData middleware.UsersData
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("received invalid users data\n")
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Invalid users data received.")
		return
	}
	json.Unmarshal(body, &usersData)

	// call backend storage and store data
	err = store.StoreUsersData(&usersData)
	if err != nil {
		log.Printf("Failed to store users data: %v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Failed to store users data.")
		return
	}

	// return data to client
	log.Printf("processed users data from %s\n", usersData.Hostname)
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-type", "application/json")
	w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	json.NewEncoder(w).Encode(usersData)
}

// updateGroups take GroupsData as JSON and store in backend
func updateGroups(w http.ResponseWriter, r *http.Request) {
	// validate API key
	apikeyClient := r.Header.Get("X-API-Key")
	if apikeyClient == "" || apikeyClient != apikey {
		log.Printf("received groups data with invalid API key\n")
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "Invalid API key.")
		return
	}

	// read POST data and convert from JSON to struct
	var groupsData middleware.GroupsData
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("received invalid groups data\n")
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Invalid groups data received.")
		return
	}
	json.Unmarshal(body, &groupsData)

	// call backend storage and store data
	err = store.StoreGroupsData(&groupsData)
	if err != nil {
		log.Printf("Failed to store groups data: %v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Failed to store groups data.")
		return
	}

	// return data to client
	log.Printf("processed groups data from %s\n", groupsData.Hostname)
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-type", "application/json")
	w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	json.NewEncoder(w).Encode(groupsData)
}

func main() {
	// create command line
	listenAddr := flag.String("listen-addr", ":9999", "Listen address and port.")
	tlsCert := flag.String("tls-cert", "server.cert", "TLS server certificate.")
	tlsKey := flag.String("tls-key", "server.key", "TLS server certificate key.")
	apiBaseURL := flag.String("api-base-url", "/api/v1", "Base URL for the API endpoint.")
	apiKey := flag.String("api-key", "", "API key to authenticate the clients.")
	mongoHost := flag.String("mongo-host", "127.0.0.1", "Hostname/IP of the MongoDB server.")
	mongoDB := flag.String("mongo-db", "computers-test", "MongoDB database name.")
	flag.Parse()

	// create a backend to store data, MongoDB in this case
	var err error
	store, err = storages.NewMongoStore(*mongoHost, *mongoDB)
	if err != nil {
		fmt.Printf("Failed to initialize MongoDB backend: %v\n", err)
		os.Exit(1)
	}
	defer store.Close()

	if *apiKey == "" {
		fmt.Printf("No API key set. Aborting.\n")
		os.Exit(1)
	}
	apikey = *apiKey

	// create the HTTP routes and listener to accept data via REST URIs
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc(*apiBaseURL+"/computer/users", updateUsers).Methods("POST")
	router.HandleFunc(*apiBaseURL+"/computer/groups", updateGroups).Methods("POST")

	// configure TLS and HTTP server settings
	tlsCfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		},
	}
	httpSrv := &http.Server{
		Addr:      *listenAddr,
		Handler:   router,
		TLSConfig: tlsCfg,
	}

	fmt.Printf("Listening on https://%s/...\n", *listenAddr+*apiBaseURL)
	err = httpSrv.ListenAndServeTLS(*tlsCert, *tlsKey)
	if err != nil {
		fmt.Printf("Failed to start server: %v\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}
