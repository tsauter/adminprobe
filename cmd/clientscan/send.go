package main

import (
	"gitlab.com/tsauter/adminprobe/middleware"
)

// A ResultSender is the interface to process the results.
type ResultSender interface {
	SendUsersData(*middleware.UsersData) error
	SendGroupsData(*middleware.GroupsData) error
}
