package main

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/tsauter/adminprobe/middleware"
)

// A RESTSender implements a ResultSender as a HTTP-REST client.
// All results will be uploaded (a.k.a. posted) to a REST API.
type RESTSender struct {
	BaseURL string
	APIKey  string
	RootCA  *x509.CertPool
}

// NewRESTSender creates a preconfigured RESTSender instance.
// The baseurl parameter specifies the REST API base URL. The sub paths
// will be automatically appended to this baseurl.
func NewRESTSender(baseurl string, cadata []byte, apikey string) (*RESTSender, error) {
	// create a new TLS root CA list with only our certificate
	rootCA := x509.NewCertPool()
	if cadata != nil {
		if ok := rootCA.AppendCertsFromPEM(cadata); !ok {
			return nil, fmt.Errorf("failed to load TLS server certificate")
		}
	}

	return &RESTSender{
		BaseURL: baseurl,
		APIKey:  apikey,
		RootCA:  rootCA,
	}, nil
}

// SendJSONData sends the data to a remote HTTP POST endpoint.
// The url will be combined with the baseurl to build the complete HTTP URI.
// Data will be send as a marshaled JSON string.
func (s *RESTSender) SendJSONData(url string, data interface{}) error {
	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(data)

	completeURL := s.BaseURL + url
	//fmt.Printf("URL: %s\n", completeURL)

	// create a POST request with the marshaled JSON data and the full URL
	req, err := http.NewRequest(http.MethodPost, completeURL, buf)
	if err != nil {
		return fmt.Errorf("failed to create HTTP REST request: %s: %v", completeURL, err)
	}

	// set authentication header
	req.Header.Set("X-API-Key", s.APIKey)

	// set content-type header
	req.Header.Set("Content-type", "application/json")

	// create TLS configuration
	// overwrite the system-wide RootCA list
	tlsTR := &http.Transport{
		TLSClientConfig: &tls.Config{
			RootCAs:                  s.RootCA,
			MinVersion:               tls.VersionTLS12,
			CurvePreferences:         []tls.CurveID{tls.CurveP521},
			PreferServerCipherSuites: true,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
			},
		},
	}

	// do the real request
	client := &http.Client{
		Transport: tlsTR,
	}
	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("failed to send HTTP REST request: %s: %v", completeURL, err)
	}
	defer res.Body.Close()

	// everything that is not a HTTP 2xx code is an error
	if (res.StatusCode < 200) || (res.StatusCode >= 299) {
		return fmt.Errorf("got error from REST API: %s", res.Status)
	}

	//fmt.Printf("REST response: %s\n", res.Status)
	log.Printf("Successfully send data to %s\n", completeURL)
	return nil
}

// SendUsersData sends UsersData{} struct to an remote HTTP REST endpoint.
// The API endpoint url must be "/computer/users".
// The data will send as they are.
func (s *RESTSender) SendUsersData(data *middleware.UsersData) error {
	return s.SendJSONData("/computer/users", data)
}

// SendGroupsData sends UsersData{} struct to an remote HTTP REST endpoint.
// The API endpoint url must be "/computer/groups".
// The data will send as they are.
func (s *RESTSender) SendGroupsData(data *middleware.GroupsData) error {
	return s.SendJSONData("/computer/groups", data)
}
