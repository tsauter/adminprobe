package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/gobuffalo/packr"
	"gopkg.in/yaml.v2"
)

// Config contains all configuration options.
type Config struct {
	Verbose     bool   `yaml:"verbose"`
	APIEndpoint string `yaml:"APIEndpoint"`
	APIKey      string `yaml:"APIKey"`
	CAFile      string `yaml:"CAFile"`
}

func main() {
	var config Config

	// parse command lines
	// before reading the config file, we may use another config file
	configfile := flag.String("config", "clientscan.yaml", "Use this configuration file")
	verbose := flag.Bool("verbose", false, "Enable verbose output.")
	flag.Parse()

	// load the configuration file from embedded packr
	// the configfile must be located in the subfolder conf/
	box := packr.NewBox("./conf")
	rawconfig, err := box.Find(*configfile)
	if err != nil {
		fmt.Printf("No configuration loaded: %v\n", err)
	}

	if len(rawconfig) > 0 {
		err = yaml.Unmarshal(rawconfig, &config)
		if err != nil {
			fmt.Printf("Failed to load YAML configuration: %v\n", err)
			os.Exit(1)
		}
		//fmt.Printf("%#v\n", config)
	}

	// overwrite configuration with command line flags
	config.Verbose = *verbose

	if config.APIEndpoint == "" {
		fmt.Printf("No API endpoint configured. Aborting.\n")
		os.Exit(1)
	}

	// load a TLS root certificate if available
	var tlsCA []byte = nil
	if config.CAFile != "" {
		tlsCA, err = box.Find(config.CAFile)
		if err != nil {
			fmt.Printf("No TLS certificate authority loaded: %s: %v\n", config.CAFile, err)
			os.Exit(1)
		}
	}

	// create a new sender that uses a REST API
	sender, err := NewRESTSender(config.APIEndpoint, tlsCA, config.APIKey)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}

	// create a proper that collect local users and groups
	prober, err := NewProber(sender)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}

	err = prober.ProbeUsers()
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}

	err = prober.ProbeGroups()
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		os.Exit(1)
	}

	os.Exit(0)
}
