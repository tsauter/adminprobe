package main

import (
	"fmt"
	"os"

	"gitlab.com/tsauter/adminprobe/middleware"
	"gitlab.com/tsauter/adminprobe/probes"
)

// A Prober represents a proper which does the main
// work to collect data.
type Prober struct {
	hostname string
	sender   ResultSender
}

// NewProber creates a preconfigured Prober instance.
// The sender is used to process the collected data
// and forward those data to the next instance (e.g. REST API).
func NewProber(sender ResultSender) (*Prober, error) {
	// get my hostname
	hostname, err := os.Hostname()
	if err != nil {
		return nil, fmt.Errorf("failed to determine my hostname: %v", err)
	}

	return &Prober{
		hostname: hostname,
		sender:   sender,
	}, nil
}

// ProbeUsers builds a list of local users, additional details about the users
// are included (SID, Disabled).
// UsersData contains all existing users in a []Users slice.
func (p *Prober) ProbeUsers() error {
	// fetch local users through the probe
	users, err := probes.GetLocalUsers()
	if err != nil {
		return fmt.Errorf("checking users failed: %v", err)
	}

	// convert the retrieved data to a generic format
	// which can be passed to the next instance
	result := middleware.UsersData{Hostname: p.hostname}

	for _, u := range *users {
		data := middleware.UserData{
			Name:     u.Name,
			Domain:   u.Domain,
			SID:      u.SID,
			Disabled: u.Disabled,
		}
		//fmt.Printf("%#v\n", data)
		result.Users = append(result.Users, data)
	}

	// send the data to the next instance
	//fmt.Printf("--> Sending: %#v\n", result)
	err = p.sender.SendUsersData(&result)
	if err != nil {
		return fmt.Errorf("submitting user results failed: %v", err)
	}

	return nil
}

// ProbeGroups builds a list of local groups and their members. Additional details
// about the groups are included (SID).
// The name of the group is translated, based on the well-known-sids, to their
// english representation (e.g. Administratoren -> Administrators).
// GroupsData contains all existing groups in a []Groups slice.
// A group is represented by GroupData. This data contains all members of this
// groups either in the []MemberUsers or []MemberGroups slice.
func (p *Prober) ProbeGroups() error {
	// fetch local groups through the probe
	groups, err := probes.GetLocalGroups()
	if err != nil {
		return fmt.Errorf("checking groups failed: %v", err)
	}

	// convert the retrieved data to a generic format
	// which can be passed to the next instance

	result := middleware.GroupsData{Hostname: p.hostname}

	for _, grp := range *groups {
		gdata := middleware.GroupData{
			Domain:  grp.Domain,
			Name:    grp.GetWellKnownName(),
			LocName: grp.Name,
			SID:     grp.SID,
		}

		// get all members of this group
		entries, err := grp.GetLocalGroupMembers()
		if err != nil {
			panic(err)
		}
		// add user members to the []MemberUsers slice
		// add group members to the []MemberGroups slice
		for _, account := range *entries {
			switch account.Type {
			case "UserAccount", "SystemAccount":
				data := middleware.UserData{
					Name:   account.Name,
					Domain: account.Domain,
				}
				//fmt.Printf("%#v\n", data)
				gdata.MemberUsers = append(gdata.MemberUsers, data)
			case "Group":
				data := middleware.GroupData{
					Name:   account.Name,
					Domain: account.Domain,
				}
				//fmt.Printf("%#v\n", data)
				gdata.MemberGroups = append(gdata.MemberGroups, data)
			default:
				return fmt.Errorf("invalid account type: %#v", account)
			}
		}

		//fmt.Printf("%#v\n", gdata)
		result.Groups = append(result.Groups, gdata)
	}

	// send the data to the next instance
	//fmt.Printf("--> Sending: %#v\n", result)
	err = p.sender.SendGroupsData(&result)
	if err != nil {
		return fmt.Errorf("submitting user results failed: %v", err)
	}

	return nil
}
