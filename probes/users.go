package probes

import (
	"fmt"

	"github.com/StackExchange/wmi"
)

// Win32_UserAccount is the WMI struct that will be filled
// with the result from the WMI query.
type Win32_UserAccount struct {
	Description  string
	Domain       string
	FullName     string
	LocalAccount bool
	Disabled     bool
	Lockout      bool
	Name         string
	SID          string
}

// GetLocalUsers query the Windows WMI interface about local users and
// return this list.
func GetLocalUsers() (*[]Win32_UserAccount, error) {
	var dst []Win32_UserAccount

	// query WMI but only local accounts, otherwise this query will
	// take ages
	q := wmi.CreateQuery(&dst, "WHERE LocalAccount=\"True\"")
	err := wmi.Query(q, &dst)
	if err != nil {
		return nil, fmt.Errorf("failed to query local users: %v", err)
	}

	return &dst, nil
}
