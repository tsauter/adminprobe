package probes

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/StackExchange/wmi"
)

var (
	// GroupComponentRegex is a regular expression to extract parts from the Win32_GroupUser PartComponent string
	GroupComponentRegex = regexp.MustCompile(`Win32_(UserAccount|Group|SystemAccount)\.Domain="(.*)",Name="(.*)"`)

	// WellKnownSIDs is a map of the pre-defined SIDs in Windows, and their original English text
	WellKnownSIDs = map[string]string{
		"S-1-5-32-544": "Administrators",
		"S-1-5-32-545": "Users",
		"S-1-5-32-546": "Guests",
		"S-1-5-32-547": "Power Users",
		"S-1-5-32-548": "Account Operators",
		"S-1-5-32-549": "Server Operators",
		"S-1-5-32-550": "Print Operators",
		"S-1-5-32-551": "Backup Operators",
		"S-1-5-32-552": "Replicators",
		"S-1-5-32-555": "Remote Desktop Users",
		"S-1-5-32-556": "Network Configuration Operators",
		"S-1-5-32-558": "Performance Monitor Users",
		"S-1-5-32-559": "Performance Log Users",
		"S-1-5-32-562": "Distributed COM Users",
		"S-1-5-32-568": "IIS_IUSRS",
		"S-1-5-32-569": "Cryptographic Operators",
		"S-1-5-32-573": "Event Log Readers",
		"S-1-5-32-578": "Hyper-V Administrators",
		"S-1-5-32-579": "Access Control Assistance Operators",
		"S-1-5-32-580": "Remote Management Users",
		"S-1-5-32-581": "System Managed Accounts Group",
		"S-1-5-32-583": "Device Owners",
	}
)

// Win32_Group is the WMI struct that will be filled
// with the result from the WMI query.
type Win32_Group struct {
	Description  string
	Domain       string
	LocalAccount bool
	Name         string
	SID          string
}

// Win32_GroupUser is the WMI struct that will be filled
// with the result from the WMI query.
// Thie is used to query members of a group.
type Win32_GroupUser struct {
	PartComponent string
}

// Account is used to hold extracted data from Win32_GroupUser
type Account struct {
	Type         string
	Domain       string
	Name         string
	LocalAccount bool
}

// GetLocalGroups query the Windows WMI interface about local users and
// return this list.
func GetLocalGroups() (*[]Win32_Group, error) {
	var dst []Win32_Group

	// get hostname
	hostname, err := os.Hostname()
	if err != nil {
		return nil, fmt.Errorf("failed to get hostname: %v", err)
	}

	// query WMI, pass the hostname as domain-name, this will return only
	// local groups, and not domain groups (which may take ages).
	q := wmi.CreateQuery(&dst, fmt.Sprintf("WHERE Domain=\"%s\"", hostname))
	err = wmi.Query(q, &dst)
	if err != nil {
		return nil, fmt.Errorf("failed to query local groups: %v", err)
	}

	return &dst, nil
}

// GetLocalGroupMembers query the Windows WMI interface about group members
// of the specified group.
// A list of member user and member groups as Account{} is returned.
func (group *Win32_Group) GetLocalGroupMembers() (*[]Account, error) {
	var entries []Account

	var dst []Win32_GroupUser

	hostname, err := os.Hostname()
	if err != nil {
		return nil, fmt.Errorf("failed to get hostname: %v", err)
	}

	qstr := fmt.Sprintf("WHERE (GroupComponent=\"Win32_Group.Name='%s',Domain='%s'\")",
		group.Name, hostname)

	q := wmi.CreateQuery(&dst, qstr)
	err = wmi.Query(q, &dst)
	if err != nil {
		return nil, fmt.Errorf("failed to query local group members: %s: %v", group.Name, err)
	}

	// walk over all returned members and try to extract details,
	// the WMI returns only an useless string
	for _, v := range dst {
		s, err := parseGroupComponents(hostname, v.PartComponent)
		if err != nil {
			return nil, err
		}

		//fmt.Printf("%s\n", s)
		entries = append(entries, s)
	}

	return &entries, nil
}

// parseGroupComponents converts a PartComponents string into an Account{} struct
func parseGroupComponents(hostname string, data string) (Account, error) {
	hostText := fmt.Sprintf("\\\\%s\\root\\cimv2:", strings.ToUpper(hostname))
	s := strings.Replace(data, hostText, "", -1)

	// Win32_UserAccount.Domain="XXXX",Name="Administrator"
	// Win32_Group.Domain="XXXDOM",Name="Domain Admins"
	// Win32_SystemAccount.Domain="XXXX",Name="INTERAKTIVE"
	match := GroupComponentRegex.FindStringSubmatch(s)
	if len(match) < 2 {
		return Account{}, fmt.Errorf("failed to extract domain/user from WMI: %s", s)
	}
	//fmt.Printf("%#v\n", match)

	return Account{
		Type:         match[1],
		Domain:       match[2],
		Name:         match[3],
		LocalAccount: (match[2] == strings.ToUpper(hostname)),
	}, nil
}

// GetWellKnownName converts the local name of a group to an English name,
// based on the well-known-sids list, defined by Microsoft.
func (group *Win32_Group) GetWellKnownName() string {
	entry, ok := WellKnownSIDs[group.SID]
	if !ok {
		entry = fmt.Sprintf("%s [%s]", group.Name, group.SID)
	}
	return entry
}

// PrettyString returns an Account{} in DOMAIN\user format.
func (a *Account) PrettyString() string {
	return fmt.Sprintf("%s\\%s", a.Domain, a.Name)
}
