package middleware

// UserData is a User with additional details.
type UserData struct {
	Domain   string
	Name     string
	SID      string
	Disabled bool
}

// GroupData is a Group with additional details.
// MembersUser contains all member users,
// MemberGroups contains all member groups.
type GroupData struct {
	Domain       string
	Name         string
	LocName      string
	SID          string
	MemberUsers  []UserData
	MemberGroups []GroupData
}

// UsersData is a container to hold details
// about the queried host and all collected users.
type UsersData struct {
	Hostname string
	Users    []UserData
}

// GroupsData is a container to hold details
// about the queried host and all collected groups.
type GroupsData struct {
	Hostname string
	Groups   []GroupData
}
